<!DOCTYPE html>
<html  lang="pl">
<html dir="ltr">


  <head>
    <meta charset="utf-8">
    <title>Blog</title>
  <link rel="stylesheet" href="css/style.css">

    <script src="js/external/jquery-2.2.2.min.js"></script>

  </head>
  <body>
  <?php include 'header.php'; ?>
    <!-- page content -->
    <div class="main">
     <div class="color-picker">
      color picker
    </div>
    <h1>Blog</h1>
<div class="blog container">
<h1>Blog</h1>
  <div class="latest-news">
    <div class="large">
      <img src="img/blank.png">
      <div class="text">
      <h2>Title</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pulvinar fringilla turpis, sed pellentesque purus ornare sed. Donec egestas dui a ipsum suscipit, et elementum massa bibendum. Praesent tempor vestibulum ex, vel tempor nisi gravida et. In in elit rhoncus dolor luctus convallis id facilisis eros. </p>
      <a href=""><button>Read more</button></a>
    </div>
    </div>
    <div class="big">
        <img src="img/blank.png">
        <div class="text">
      <h2>Title</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pulvinar fringilla turpis, sed pellentesque purus ornare sed. Donec egestas dui a ipsum suscipit, et elementum massa bibendum. Praesent tempor vestibulum ex, vel tempor nisi gravida et. In in elit rhoncus dolor luctus convallis id facilisis eros. </p>
      <a href=""><button>Read more</button></a>
    </div>
    </div>
     <div class="big">
        <img src="img/blank.png">
        <div class="text">
      <h2>Title</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pulvinar fringilla turpis, sed pellentesque purus ornare sed. Donec egestas dui a ipsum suscipit, et elementum massa bibendum. Praesent tempor vestibulum ex, vel tempor nisi gravida et. In in elit rhoncus dolor luctus convallis id facilisis eros. </p>
      <a href=""><button>Read more</button></a>
    </div>
    </div>
  </div>
  <div class="news">
    <div class="item">
       <img src="img/blank.png">
        <div class="text">
      <h2>Title</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pulvinar fringilla turpis, sed pellentesque purus ornare sed. Donec egestas dui a ipsum suscipit, et elementum massa bibendum. Praesent tempor vestibulum ex, vel tempor nisi gravida et. In in elit rhoncus dolor luctus convallis id facilisis eros. </p>
      <a href=""><button>Read more</button></a>
    </div>
    </div>
     <div class="item">
       <img src="img/blank.png">
        <div class="text">
      <h2>Title</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pulvinar fringilla turpis, sed pellentesque purus ornare sed. Donec egestas dui a ipsum suscipit, et elementum massa bibendum. Praesent tempor vestibulum ex, vel tempor nisi gravida et. In in elit rhoncus dolor luctus convallis id facilisis eros. </p>
      <a href=""><button>Read more</button></a>
    </div>
    </div>
     <div class="item">
       <img src="img/blank.png">
        <div class="text">
      <h2>Title</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pulvinar fringilla turpis, sed pellentesque purus ornare sed. Donec egestas dui a ipsum suscipit, et elementum massa bibendum. Praesent tempor vestibulum ex, vel tempor nisi gravida et. In in elit rhoncus dolor luctus convallis id facilisis eros. </p>
      <a href=""><button>Read more</button></a>
    </div>
    </div>
    <button class="load-more">Load more</button>
  </div>
</div>
</div>
  <?php include 'footer.php'; ?>
  </body>
</html>