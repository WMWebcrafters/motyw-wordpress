<!DOCTYPE html>
<html  lang="pl">
<html dir="ltr">


  <head>
    <meta charset="utf-8">
    <title>About us</title>
  <link rel="stylesheet" href="css/style.css">

    <script src="js/external/jquery-2.2.2.min.js"></script>

  </head>
  <body>
  <?php include 'header.php'; ?>
    <!-- page content -->
    <div class="about-us main">
     <div class="color-picker">
      color picker
    </div>
    <div class="lead">
    <div class="container">
      <h1>Duis vulputate</h1>
      <p>Phasellus tempor, augue non ullamcorper tempus, arcu arcu mattis magna, non malesuada nisl enim in leo. Vestibulum ante leo, viverra vitae vulputate finibus, condimentum in massa. Curabitur ut felis molestie, blandit nibh at, malesuada sapien. Praesent sodales scelerisque lectus at vestibulum. </p>
    </div>
</div>
<div class="info-boxes">
<div class="container">
  <div class="box">
 (ikonka)
    <h2>Nulla vel metus nec odio</h2>
    <p>Fusce arcu lectus, lobortis fringilla velit vel, suscipit dignissim dolor. Etiam libero tortor, rutrum quis lacus ut, pharetra bibendum arcu. Morbi at urna quis dui suscipit eleifend ac eu purus.</p>
  </div>
  <div class="box">
   (ikonka)
    <h2>Nulla vel metus nec odio</h2>
    <p>Fusce arcu lectus, lobortis fringilla velit vel, suscipit dignissim dolor. Etiam libero tortor, rutrum quis lacus ut, pharetra bibendum arcu. Morbi at urna quis dui suscipit eleifend ac eu purus.</p>
  </div>
  <div class="box">
   (ikonka)
    <h2>Nulla vel metus nec odio</h2>
    <p>Fusce arcu lectus, lobortis fringilla velit vel, suscipit dignissim dolor. Etiam libero tortor, rutrum quis lacus ut, pharetra bibendum arcu. Morbi at urna quis dui suscipit eleifend ac eu purus.</p>
  </div>
</div>
</div>
<div class="team">
<div class="container">
  <div class="member">
    <img src="img/blank.png" alt="">
    <h2>Name</h2>
    <h3>Job title</h3>
    <p>Nulla vel metus nec odio eleifend tristique id sed velit. Nullam venenatis nulla id luctus fermentum. Sed sed aliquet urna. Nulla mollis neque sit amet gravida cursus.</p>
  </div>
   <div class="member">
    <img src="img/blank.png" alt="">
    <h2>Name</h2>
    <h3>Job title</h3>
    <p>Nulla vel metus nec odio eleifend tristique id sed velit. Nullam venenatis nulla id luctus fermentum. Sed sed aliquet urna. Nulla mollis neque sit amet gravida cursus.</p>
  </div>
   <div class="member">
    <img src="img/blank.png" alt="">
    <h2>Name</h2>
    <h3>Job title</h3>
    <p>Nulla vel metus nec odio eleifend tristique id sed velit. Nullam venenatis nulla id luctus fermentum. Sed sed aliquet urna. Nulla mollis neque sit amet gravida cursus.</p>
  </div>
   <div class="member">
    <img src="img/blank.png" alt="">
    <h2>Name</h2>
    <h3>Job title</h3>
    <p>Nulla vel metus nec odio eleifend tristique id sed velit. Nullam venenatis nulla id luctus fermentum. Sed sed aliquet urna. Nulla mollis neque sit amet gravida cursus.</p>
  </div>
</div>
</div>
<div class="testimonials">
<div class="container">
<div class="single">
<div class="photo">
  <img src="img/blank.png" alt="">
</div>
<div class="testimonial">
  <h2>Name</h2>
  <h3>Who is this person</h3>
  <p>In tincidunt leo feugiat nisl accumsan porta. Fusce arcu lectus, lobortis fringilla velit vel, suscipit dignissim dolor. Etiam libero tortor, rutrum quis lacus ut, pharetra bibendum arcu. Morbi at urna quis dui suscipit eleifend ac eu purus. Nulla pulvinar diam nunc, quis lacinia lacus maximus id. Duis malesuada quis nulla ac finibus. Etiam vitae maximus erat, eu blandit elit.</p>
</div>
</div>
</div>
</div>
</div>
  <?php include 'footer.php'; ?>
  </body>
</html>