<!DOCTYPE html>
<html  lang="pl">
<html dir="ltr">


  <head>
    <meta charset="utf-8">
    <title>Homepage</title>
  <link rel="stylesheet" href="css/style.css">

    <script src="js/external/jquery-2.2.2.min.js"></script>

  </head>
  <body>
 
    <!-- page content -->
 <?php include 'header.php'; ?>
    <!-- page content -->
    <div class="homepage main">
    <div class="color-picker">
    	color picker
    </div>
    <div class="slider">
    <div class="caption">
    	Slider
    	</div>
    	<div class="scroller">
    		scroller
    	</div>
    </div>
    <div class="about-us-preview">
    <div class="container">
    	<h1>About us</h1>
    	<div class="info">
    		<h2>Lorem ipsum</h2>
    		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu volutpat massa, in mattis neque. Vestibulum commodo quis magna ac cursus. Quisque non metus blandit, euismod arcu iaculis, mattis tellus. Praesent imperdiet quam vel risus hendrerit scelerisque. Donec ac dignissim mauris. Proin condimentum dui id massa aliquam varius. Integer ut accumsan enim. Donec ultrices ultrices diam, nec accumsan felis interdum vitae.</p>
    	</div>
    	<div class="media">
    		<img src="img/blank.png" alt="">
    	</div>
    	</div>
    	<a href="about-us.php"><button class="standard-button">Zobacz więcej</button></a>
    </div>
    <div class="gallery-portfolio-preview">
    	<h1>Gallery/Portfolio</h1>
    	<div class="container">
    	<div class="grid">
    	
    		<div class="item">
    		<a href="">
    			<img src="img/blank.png" alt="">
    			</a>
    		</div>
    		<div class="item">
    		<a href="">
    			<img src="img/blank.png" alt="">
    			</a>
    		</div>
    		<div class="item">
    		<a href="">
    			<img src="img/blank.png" alt="">
    			</a>
    		</div>
    		<div class="item">
    		<a href="">
    			<img src="img/blank.png" alt="">
    			</a>
    		</div>
    		<div class="item">
    		<a href="">
    			<img src="img/blank.png" alt="">
    			</a>
    		</div>
    		<div class="item">
    		<a href="">
    			<img src="img/blank.png" alt="">
    			</a>
    		</div>
    		<div class="item">
    		<a href="">
    			<img src="img/blank.png" alt="">
    			</a>
    		</div>
    		<div class="item">
    		<a href="">
    			<img src="img/blank.png" alt="">
    			</a>
    		</div>
    		

    	</div>
    	</div>
    	<a href="gallery.php"><button class="standard-button">Zobacz więcej</button></a>
    </div>
    <div class="blog-preview">
    <div class="container">
    <h1>Blog</h1>
    <p>Curabitur tincidunt laoreet mauris, quis dictum sem feugiat eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean condimentum ullamcorper urna et bibendum. Aenean aliquam orci accumsan odio molestie, nec dignissim velit consequat. Phasellus molestie elementum lectus.</p>
    </div>
    	<a href="blog.php"><button class="standard-button">Zobacz więcej</button></a>
    </div>


</div>
  <?php include 'footer.php'; ?>

  </body>
</html>